import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GinDrinkComponent} from './components/gin-drink/gin-drink.component';
import {GreenMexicanComponent} from './components/green-mexican/green-mexican.component';
import {SambukaComponent} from './components/sambuka-drink/sambuka.component';
import {VineDrinkComponent} from './components/vine-drink/vine-drink.component';
import {BeerDrinkComponent} from './components/beer-drink/beer-drink.component';
import {DrinkRouting} from './drink.routing';
import {DrinkComponent} from './drink.component';

@NgModule({
  imports: [CommonModule, DrinkRouting],
  declarations: [
    BeerDrinkComponent,
    GinDrinkComponent,
    GreenMexicanComponent,
    SambukaComponent,
    VineDrinkComponent,
    DrinkComponent
  ]
})

export class DrinkModule {

}
