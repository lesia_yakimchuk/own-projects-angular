import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SambukaComponent } from './sambuka.component';

describe('SambukaComponent', () => {
  let component: SambukaComponent;
  let fixture: ComponentFixture<SambukaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SambukaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SambukaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
