import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GinDrinkComponent } from './gin-drink.component';

describe('GinDrinkComponent', () => {
  let component: GinDrinkComponent;
  let fixture: ComponentFixture<GinDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GinDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GinDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
