import {RouterModule, Routes} from '@angular/router';
import {BeerDrinkComponent} from './components/beer-drink/beer-drink.component';
import {GinDrinkComponent} from './components/gin-drink/gin-drink.component';
import {SambukaComponent} from './components/sambuka-drink/sambuka.component';
import {VineDrinkComponent} from './components/vine-drink/vine-drink.component';
import {GreenMexicanComponent} from './components/green-mexican/green-mexican.component';
import {NgModule} from '@angular/core';
import {DrinkComponent} from './drink.component';


export const DRINK_CHILD_ROUTES: Routes = [
  {
    path: '', component: DrinkComponent, children: [

  {path: 'beer', component: BeerDrinkComponent},
  {path: 'gin', component: GinDrinkComponent},
  {path: 'sambuka', component: SambukaComponent},
  {path: 'vine', component: VineDrinkComponent},
  {path: 'greenMexican', component: GreenMexicanComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(DRINK_CHILD_ROUTES)],
  exports: [RouterModule]
})

export class DrinkRouting {}

