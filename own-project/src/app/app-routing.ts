import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import {DrinkComponent} from './drink/drink.component';
import {GenderDataComponent} from './components/gender-data/gender-data.component';
import {WeightDataComponent} from './components/weight-data/weight-data.component';
import {DrinkRouting} from './drink/drink.routing';


const routes: Routes = [
  {path: '', component: GenderDataComponent},
  {path: 'weight', component: WeightDataComponent},
   // {path: 'drink', component: DrinkComponent, children: DRINK_CHILD_ROUTES},
  {path: 'drink', loadChildren: 'app/drink/drink.module#DrinkModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting { }


