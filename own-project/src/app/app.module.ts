import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRouting } from './app-routing';
import { AppComponent } from './app.component';
// import { GenderDataComponent } from './components/gender-data/gender-data.component';
import { DrinkComponent } from './drink/drink.component';
import {FormsModule} from '@angular/forms';
import {DrinkModule} from './drink/drink.module';
// import {WeightDataComponent} from './components/weight-data/weight-data.component';
// import {CoreModule} from './core/core.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
