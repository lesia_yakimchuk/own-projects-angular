import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-gender-data',
  templateUrl: './gender-data.component.html',
  styleUrls: ['./gender-data.component.scss']
})
export class GenderDataComponent {

  constructor(private router: Router) { }

  public onNavigate() {
    // noinspection JSIgnoredPromiseFromCall
    this.router.navigate(['weight']);
  }

}
// describe confirm about age 18+
