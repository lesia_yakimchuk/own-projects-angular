import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-weight-data',
  templateUrl: './weight-data.component.html',
  styleUrls: ['./weight-data.component.scss']
})
export class WeightDataComponent {

  constructor(private router: Router) {
  }

  public navigateToDrink() {
    this.router.navigate(['drink']);
  }
}

