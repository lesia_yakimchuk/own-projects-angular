import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { GenderDataComponent } from './components/gender-data/gender-data.component';
import { WeightDataComponent } from './components/weight-data/weight-data.component';
import {DrinkDataModule} from './drink-data/drink-data.module';
import {AppRouting} from './app.routing';


@NgModule({
  declarations: [
    AppComponent,
    GenderDataComponent,
    WeightDataComponent
  ],
  imports: [
    BrowserModule,
    // DrinkDataModule,
    AppRouting
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
