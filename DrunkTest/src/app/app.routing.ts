import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GenderDataComponent} from './components/gender-data/gender-data.component';
import {WeightDataComponent} from './components/weight-data/weight-data.component';


const routes: Routes = [
  {path: '', component: GenderDataComponent},
  {path: 'weight', component: WeightDataComponent},
  {path: 'drink', loadChildren: 'app/drink-data/drink-data.module#DrinkDataModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
  export class AppRouting { }
