import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-weight-data',
  templateUrl: './weight-data.component.html',
  styleUrls: ['./weight-data.component.css']
})
export class WeightDataComponent {

  constructor(public router: Router) { }

  public navigateToDrink() {
    this.router.navigate(['drink']);
  }

  public navigateToGender() {
    this.router.navigate(['gender']);
  }
}
