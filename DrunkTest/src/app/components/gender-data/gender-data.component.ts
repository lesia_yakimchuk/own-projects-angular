import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gender-data',
  templateUrl: './gender-data.component.html',
  styleUrls: ['./gender-data.component.css']
})
export class GenderDataComponent {

  constructor(public router: Router) { }

  public navigateToWeight() {
    this.router.navigate(['weight']);
  }

}
