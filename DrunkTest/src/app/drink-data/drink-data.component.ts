import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-drink-data',
  templateUrl: './drink-data.component.html',
  styleUrls: ['./drink-data.component.css']
})
export class DrinkDataComponent {

  constructor(private router: Router) { }

  public getResult () {
    this.router.navigate([]);
  }

}
