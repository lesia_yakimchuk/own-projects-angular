import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrinkDataComponent } from './drink-data.component';
import { BeerDrinkComponent } from './components/beer-drink/beer-drink.component';
import { GinDrinkComponent } from './components/gin-drink/gin-drink.component';
import { SambukaDrinkComponent } from './components/sambuka-drink/sambuka-drink.component';
import { VineDrinkComponent } from './components/vine-drink/vine-drink.component';
import {DrinkDataRouting} from './drink-data.routing';
import {GreenMexicanComponent} from './components/green-mexican/green-mexican.component';

@NgModule({
  declarations: [DrinkDataComponent, BeerDrinkComponent, GinDrinkComponent, SambukaDrinkComponent, VineDrinkComponent, GreenMexicanComponent],
  imports: [
    CommonModule, DrinkDataRouting
  ]
})
export class DrinkDataModule { }
