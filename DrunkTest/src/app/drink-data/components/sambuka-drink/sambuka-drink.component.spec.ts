import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SambukaDrinkComponent } from './sambuka-drink.component';

describe('SambukaDrinkComponent', () => {
  let component: SambukaDrinkComponent;
  let fixture: ComponentFixture<SambukaDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SambukaDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SambukaDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
