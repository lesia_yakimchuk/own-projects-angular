import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenMexicanComponent } from './green-mexican.component';

describe('GreenMexicanComponent', () => {
  let component: GreenMexicanComponent;
  let fixture: ComponentFixture<GreenMexicanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenMexicanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenMexicanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
