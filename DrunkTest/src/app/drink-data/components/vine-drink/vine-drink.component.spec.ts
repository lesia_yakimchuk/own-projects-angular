import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VineDrinkComponent } from './vine-drink.component';

describe('VineDrinkComponent', () => {
  let component: VineDrinkComponent;
  let fixture: ComponentFixture<VineDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VineDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VineDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
