import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerDrinkComponent } from './beer-drink.component';

describe('BeerDrinkComponent', () => {
  let component: BeerDrinkComponent;
  let fixture: ComponentFixture<BeerDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
