import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkDataComponent } from './drink-data.component';

describe('DrinkDataComponent', () => {
  let component: DrinkDataComponent;
  let fixture: ComponentFixture<DrinkDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrinkDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
