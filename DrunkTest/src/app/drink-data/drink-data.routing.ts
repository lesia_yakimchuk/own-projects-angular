import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {DrinkDataComponent} from './drink-data.component';
import {BeerDrinkComponent} from './components/beer-drink/beer-drink.component';
import {GinDrinkComponent} from './components/gin-drink/gin-drink.component';
import {GreenMexicanComponent} from './components/green-mexican/green-mexican.component';
import {SambukaDrinkComponent} from './components/sambuka-drink/sambuka-drink.component';
import {VineDrinkComponent} from './components/vine-drink/vine-drink.component';



 const USER_CHILD_ROUTES: Routes = [
  {
    path: '', component: DrinkDataComponent, children: [
      {path: 'beer', component: BeerDrinkComponent},
      {path: 'gin', component: GinDrinkComponent},
      {path: 'green-mexican', component: GreenMexicanComponent},
      {path: 'sambuka', component: SambukaDrinkComponent},
      {path: 'vine', component: VineDrinkComponent}
    ]
   }
 ];

@NgModule({
   imports: [RouterModule.forChild(USER_CHILD_ROUTES)],
   exports: [RouterModule]
 })
 export class DrinkDataRouting {
 }
